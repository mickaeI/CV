// Chargement de la page 
$(document).ready(function() {
	setTimeout(function(){
		$('body').addClass('loaded');
		$('h1').css('color','#222222');
  }, 0); // Régler durée minimale du chargement
});

// Eléments Materialize
$(document).ready(function(){
  $('.sidenav').sidenav();
});

$(document).ready(function(){
  $('.parallax').parallax();
});

$(document).ready(function(){
  $('.scrollspy').scrollSpy();
});

$(document).ready(function(){
  $('.materialboxed').materialbox();
});

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('.fixed-action-btn');
  var instances = M.FloatingActionButton.init(elems, {
    direction: 'top',
    hoverEnabled: false
  });
});

$('.carousel.carousel-slider').carousel({
  fullWidth: true,
  indicators: true
});

